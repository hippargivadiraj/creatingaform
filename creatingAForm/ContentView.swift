//
//  ContentView.swift
//  creatingAForm
//
//  Created by Leadconsultant on 11/13/19.
//  Copyright © 2019 Leadconsultant. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
